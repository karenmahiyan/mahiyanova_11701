package homework;

import java.util.regex.*;

public class Regex5 {
	public static void main (String [] args) {
		Pattern p = Pattern.compile("[0-2][0-9]:[0-5][0-9]:[0-5][0-9]");
		for (String n: args) {
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}
