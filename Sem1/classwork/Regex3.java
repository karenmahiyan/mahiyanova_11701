package homework;

import java.util.regex.*;

public class Regex3 {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("[1][0-9]+{3}");
		Matcher m = p.matcher("1001 2645 1645 2742 1009 1964 1999 2000 2034 2001 1000");
	    while (m.find()) {
		System.out.println(m.group());
	}
    }
}