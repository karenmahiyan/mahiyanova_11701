package homework;

import java.util.regex.*;

public class Regex6 {
	public static void main(String[] args) {
		Pattern p = Pattern.compile("[А, В, Е, К, М, Н, О, Р, С, Т, У, Х][0-9][0-9][0-9][А, В, Е, К, М, Н, О, Р, С, Т, У, Х][А, В, Е, К, М, Н, О, Р, С, Т, У, Х]");
		Matcher m = p.matcher("К445АР Щ657ЪЪ А76ОО Ы02НЕ Х658СТ К679РР");
		while (m.find()) {
			System.out.println(m.group());
		}
	}

}
