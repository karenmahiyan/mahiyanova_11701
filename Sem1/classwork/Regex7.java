package homework;

import java.util.regex.*;

public class Regex7 {

	public static void main(String[] args) {
		Pattern p = Pattern.compile("[+7]-[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]");
		Matcher m = p.matcher("+7-456-665-76-76 +7-999-545-55-65 8-945-566-34-35");
		while(m.find()) {
			System.out.println(m.group());
		}
	}

}
