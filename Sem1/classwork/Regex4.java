package homework;

import java.util.regex.*;

public class Regex4 {
	public static void main (String [] args) {
		Pattern p = Pattern.compile("1(4(3[2-9]|[4-9]\\\\d)|[5-9]\\\\d{2})|2([0-2]\\\\d{2}|3([0-6]\\\\d|7[0-5]))");
		for (String n: args) {
			Matcher m = p.matcher(n);
			System.out.println(m.matches());
		}
	}
}
