package homework;

import java.util.regex.*;

public class Regex2 {
	public static void main(String [] args) {
		Pattern p = Pattern.compile ("[-]?[1-9][0-9]*");
		Matcher m = p.matcher("2 74 3.14 100 008 52.934 -5");
		while (m.find()) {
			System.out.println(m.group());
		}
	}
}
