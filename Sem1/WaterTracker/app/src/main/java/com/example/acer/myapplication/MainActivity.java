package com.example.acer.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button yesbutton;
    Button nobutton;
    Button calendarbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.calendarbutton).setOnClickListener(this);
        findViewById(R.id.nobutton).setOnClickListener(this);
        findViewById(R.id.yesbutton).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.calendarbutton:
                startActivity(new Intent(this, Main2Activity.class));
                break;
            case R.id.nobutton:
                PrefUtils.saveDate(this, Calendar.getInstance().getTimeInMillis(), false);
                break;
            case R.id.yesbutton:
                PrefUtils.saveDate(this, Calendar.getInstance().getTimeInMillis(), true);
                break;
        }
    }
}
