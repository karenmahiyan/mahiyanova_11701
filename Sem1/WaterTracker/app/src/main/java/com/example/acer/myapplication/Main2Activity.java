package com.example.acer.myapplication;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.roomorama.caldroid.CaldroidFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Main2Activity extends AppCompatActivity {

    private CaldroidFragment caldroidFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        setupCalendar();
    }

    private void setupCalendar(){
        final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        caldroidFragment = new CaldroidFragment();

        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
        setCustomResourceForDates();
        caldroidFragment.setArguments(args);

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();
    }

    private void setCustomResourceForDates() {
        if (caldroidFragment == null)
            return;
//        String dates = "1512864000\ntrue\n1512864000\nfalse\n1512432000\nfalse";
        String dates = PrefUtils.getDates(this);
        if(dates.equals(""))
            return;
        String[] datesArr = dates.split("\n");
        for (int i = 0; i < datesArr.length; i += 2) {
            Date customDate = getCustomDate(Long.parseLong(datesArr[i]));

            ColorDrawable color = new ColorDrawable(DateType.getEnum(Boolean.parseBoolean(datesArr[i + 1])).color);
            caldroidFragment.setBackgroundDrawableForDate(color, customDate);
            caldroidFragment.setTextColorForDate(R.color.caldroid_white, customDate);
        }

    }

    private Date getCustomDate(long x){
        Date blueDate = new Date(x);
        return blueDate;
    }

    private enum DateType {
        YES_DAY(Color.GREEN, true),
        NO_DAY(Color.RED, false);

        private int color;

        private boolean flag;

        DateType(int color, boolean flag) {
            this.color = color;
            this.flag = flag;
        }

        public static DateType getEnum(boolean flag){
            for (DateType type : DateType.values()) {
                if(type.flag == flag){
                    return type;
                }
            }
            throw new IllegalArgumentException();
        }
    }
}
