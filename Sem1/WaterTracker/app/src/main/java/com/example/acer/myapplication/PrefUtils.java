package com.example.acer.myapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import java.util.Locale;

public final class PrefUtils {

    private static final String PREF_DATES = "pref_dates";

    private PrefUtils() {
        throw new IllegalStateException("Final class can not be instantiated");
    }

    public static String getDates(@NonNull Context context) {
        return getApplicationPreferences(context).getString(PREF_DATES, "");
    }

    public static void saveDate(@NonNull final Context context, long unixTime, boolean flag) {
        String allDates = getDates(context);
        if(!allDates.equals("")) allDates += "\n";
        allDates += String.format(Locale.getDefault(),"%d\n%s", unixTime, String.valueOf(flag));
        getApplicationEditor(context).putString(PREF_DATES, allDates).apply();
    }

    public static void clearPreferences(@NonNull Context context) {
        getApplicationEditor(context).clear().apply();
    }

    private static SharedPreferences.Editor getEditor(@NonNull Context context) {
        return getPreferences(context).edit();
    }

    private static SharedPreferences getPreferences(@NonNull Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static SharedPreferences getApplicationPreferences(@NonNull Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    private static SharedPreferences.Editor getApplicationEditor(@NonNull Context context) {
        return getApplicationPreferences(context).edit();
    }
}
