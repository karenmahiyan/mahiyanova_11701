package TASK05PS3;

public class MatrixException extends Exception {

    public MatrixException (){
        super("Determinant is zero");
    }
}
