package TASK05PS3;

import TASK03PS3.Vector2D;

public class Matrix2x2 {
    private double[][] matrix;

    public Matrix2x2() {
        this.matrix = new double[2][2];
        this.matrix[0][0] = 0;
        this.matrix[0][1] = 0;
        this.matrix[1][0] = 0;
        this.matrix[1][1] = 0;
    }

    public Matrix2x2(double d) {
        this.matrix = new double[2][2];
        this.matrix[0][0] = d;
        this.matrix[0][1] = d;
        this.matrix[1][0] = d;
        this.matrix[1][1] = d;
    }

    public Matrix2x2(double[][] d) {
        this.matrix = d;
    }

    public Matrix2x2(double a, double b, double c, double d) {
        this.matrix = new double[2][2];
        this.matrix[0][0] = a;
        this.matrix[0][1] = b;
        this.matrix[1][0] = c;
        this.matrix[1][1] = d;
    }

    public Matrix2x2 add(Matrix2x2 m) {
        Matrix2x2 mat = new Matrix2x2();
        mat.matrix[0][0] = this.matrix[0][0] + m.matrix[0][0];
        mat.matrix[0][1] = this.matrix[0][1] + m.matrix[0][1];
        mat.matrix[1][0] = this.matrix[1][0] + m.matrix[1][0];
        mat.matrix[1][1] = this.matrix[1][1] + m.matrix[1][1];
        return mat;
    }

    public Matrix2x2 sub(Matrix2x2 m) {
        Matrix2x2 mat = new Matrix2x2();
        mat.matrix[0][0] = this.matrix[0][0] - m.matrix[0][0];
        mat.matrix[0][1] = this.matrix[0][1] - m.matrix[0][1];
        mat.matrix[1][0] = this.matrix[1][0] - m.matrix[1][0];
        mat.matrix[1][1] = this.matrix[1][1] - m.matrix[1][1];
        return mat;
    }

    public Matrix2x2 mult(double num) {
        Matrix2x2 mat = new Matrix2x2();
        mat.matrix[0][0] = this.matrix[0][0] * num;
        mat.matrix[0][1] = this.matrix[0][1] * num;
        mat.matrix[1][0] = this.matrix[1][0] * num;
        mat.matrix[1][1] = this.matrix[1][1] * num;
        return mat;
    }

    public Matrix2x2 mult(Matrix2x2 m) {
        Matrix2x2 mat = new Matrix2x2();
        mat.matrix[0][0] = this.matrix[0][0] * m.matrix[0][0] + this.matrix[0][1] * m.matrix[1][0];
        mat.matrix[0][1] = this.matrix[0][0] * m.matrix[1][0] + this.matrix[0][1] * m.matrix[1][1];
        mat.matrix[1][0] = this.matrix[1][0] * m.matrix[0][0] + this.matrix[0][1] * m.matrix[1][0];
        mat.matrix[1][1] = this.matrix[1][1] * m.matrix[1][0] + this.matrix[0][1] * m.matrix[1][1];
        return mat;
    }

    public double det() {
        return this.matrix[0][0] * this.matrix[1][1] - this.matrix[1][0] * this.matrix[0][1];
    }

    public void transpose() {
        Matrix2x2 mat = new Matrix2x2();
        mat.matrix[0][0] = this.matrix[0][0];
        mat.matrix[0][1] = this.matrix[1][0];
        mat.matrix[1][0] = this.matrix[0][1];
        mat.matrix[1][1] = this.matrix[0][0];
        this.matrix[0][0] = mat.matrix[0][0];
        this.matrix[0][1] = mat.matrix[0][1];
        this.matrix[1][0] = mat.matrix[1][0];
        this.matrix[1][1] = mat.matrix[1][1];
    }

    public Matrix2x2 algCom() {
        Matrix2x2 mat = new Matrix2x2();
        mat.matrix[0][0] = this.matrix[1][1];
        mat.matrix[0][1] = -this.matrix[1][0];
        mat.matrix[1][0] = -this.matrix[0][1];
        mat.matrix[1][1] = this.matrix[0][0];

        return mat;
    }

    public Matrix2x2 inverseMatrix() throws MatrixException {
        double a = this.det();
        if(a==0){
            throw new MatrixException();
        }
        a = 1 / a;
        Matrix2x2 result = this.algCom().mult(a);
        result.transpose();
        return result;
    }

    public Vector2D multVector(Vector2D vector2D){
        Vector2D result = new Vector2D(this.matrix[0][0]*vector2D.getX()+this.matrix[0][1]*vector2D.getY(),
                this.matrix[1][0]*vector2D.getX()+this.matrix[1][1]*vector2D.getY());
        return result;
    }

    public static void main(String[] args) throws MatrixException {
        Matrix2x2 matrix = new Matrix2x2(1,5,9,0);
        matrix = matrix.add(matrix);
        matrix = matrix.mult(5);
        System.out.println(matrix.det());
        matrix = matrix.inverseMatrix();
        Vector2D vector2D = new Vector2D(5,1);
        vector2D = matrix.multVector(vector2D);
        System.out.println(vector2D);
    }
}
