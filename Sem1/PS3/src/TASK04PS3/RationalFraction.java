package TASK04PS3;

public class RationalFraction {
    private int a;
    private int b;


    public RationalFraction() {
        a = 0;
        b = 1;
    }

    public RationalFraction(int a, int b) {
        try {
            this.a = a;
            this.b = b;
            int test = a / b;

        } catch (ArithmeticException ex) {
            System.out.println("Division by Zero");
        }
    }

    public void reduce() {
        int num = this.a;
        int num1 = this.b;
        while ( num != 0 && num1 != 0 ) {
            if (num > num1) {
                num = num % num1;
            } else {
                num1 = num1 % num;
            }
        }
        this.a = this.a/(num+num1);
        this.b = this.b/(num+num1);
    }

    public RationalFraction add(RationalFraction rFraction){
        RationalFraction result;
        int resA = this.getA()*rFraction.getB()+rFraction.getA()*this.getB();
        int resB = this.getB()*rFraction.getB();
        result = new RationalFraction(resA,resB);
        result.reduce();
        return result;
    }

    public RationalFraction sub(RationalFraction rFraction){
        RationalFraction result;
        int resA = this.getA()*rFraction.getB()-rFraction.getA()*this.getB();
        int resB = this.getB()*rFraction.getB();
        result = new RationalFraction(resA,resB);
        result.reduce();
        return result;
    }

    public RationalFraction mult(RationalFraction rFraction){
        RationalFraction result = new RationalFraction(this.getA()*rFraction.getA(), this.getB()*rFraction.getB());
        result.reduce();
        return result;
    }

    public RationalFraction div(RationalFraction rFraction){
        RationalFraction result = new RationalFraction(this.getA()*rFraction.getB(), this.getB()*rFraction.getA());
        result.reduce();
        return result;
    }

    public String toString(){
        return (this.getA()+"\\"+this.getB());
    }

    public double value(){
        return (this.getA()/this.getB());
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public static void main(String[] args) {
        RationalFraction rFraction1 = new RationalFraction(1,2);
        RationalFraction rFraction2 = rFraction1.add(rFraction1);
        System.out.println(rFraction2);
        rFraction2 = rFraction2.sub(rFraction1);
        System.out.println(rFraction2);
        rFraction1 = rFraction1.mult(rFraction2);
        System.out.println(rFraction1);
        rFraction1 = rFraction1.div(rFraction1);
        System.out.println(rFraction1);
        System.out.println(rFraction1.value());
        System.out.println(rFraction1.toString());
    }


}
