package TASK01PS3;

import java.util.Scanner;

public class Game {
    public static void play() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите имя Игрока 1:");
        Player player1 = new Player(sc.nextLine());
        System.out.println("Введите имя Игрока 2:");
        Player player2 = new Player(sc.nextLine());
        boolean next = true;
        while ( player1.getHp() > 0 & player2.getHp() > 0 ) {
            if (next) {
                System.out.println(player1.getName() + " введите силу удара:");
                player1.attack(player2,sc.nextInt());
                System.out.println(player2.getName()+" здоровье "+player2.getHp());
                next = false;
            } else {
                System.out.println(player2.getName() + " введите силу удара:");
                player2.attack(player1,sc.nextInt());
                System.out.println(player1.getName()+" здоровье "+player1.getHp());
                next = true;
            }
        }
        if(player1.getHp()<=0){
            System.out.println("Победитель: "+player2.getName());
        } else {
            System.out.println("Победитель: "+player1.getName());
        }
    }
}
