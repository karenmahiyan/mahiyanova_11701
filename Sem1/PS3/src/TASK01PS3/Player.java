package TASK01PS3;

public class Player {
    private String name;
    private int hp = 100;

    public Player(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void attack(Player pl, int power) {
        int random = (int) (Math.random() * 9);
        if (power > random) {
            power = 0;
        }
        pl.setHp(pl.getHp() - power);
    }

    public String getName() {
        return name;
    }
}
