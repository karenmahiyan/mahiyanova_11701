package TASK07PS3;
package cwork1;

import TASK04PS3.RationalFraction;
import TASK06PS3.RationalVector2D;

public class RationalMatrix2x2 {
    private RationalFraction[][] matrix;

    public RationalMatrix2x2() {
        this.matrix = new RationalFraction[2][2];
        this.matrix[0][0] = new RationalFraction();
        this.matrix[0][1] = new RationalFraction();
        this.matrix[1][0] = new RationalFraction();
        this.matrix[1][1] = new RationalFraction();
    }
    public RationalMatrix2x2(RationalFraction rationalFraction){
        this.matrix = new RationalFraction[2][2];
        this.matrix[0][0] = rationalFraction;
        this.matrix[0][1] = rationalFraction;
        this.matrix[1][0] = rationalFraction;
        this.matrix[1][1] = rationalFraction;
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 m){
        RationalMatrix2x2 mat = new RationalMatrix2x2();
        mat.matrix[0][0] = this.matrix[0][0].add(m.matrix[0][0]);
        mat.matrix[0][1] = this.matrix[0][1].add(m.matrix[0][1]);
        mat.matrix[1][0] = this.matrix[1][0].add(m.matrix[1][0]);
        mat.matrix[1][1] = this.matrix[1][1].add(m.matrix[1][1]);
        return mat;
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 m){
        RationalMatrix2x2 mat = new RationalMatrix2x2();
        mat.matrix[0][0] = this.matrix[0][0].mult(matrix[0][0]).add(this.matrix[0][1].mult(m.matrix[1][0]));
        mat.matrix[0][1] = this.matrix[0][0].mult(m.matrix[1][0]).add(this.matrix[0][1].mult(m.matrix[1][1]));
        mat.matrix[1][0] = this.matrix[1][0].mult(m.matrix[0][0]).add(this.matrix[0][1].mult(m.matrix[1][0]));
        mat.matrix[1][1] = this.matrix[1][1].mult(m.matrix[1][0]).add(this.matrix[0][1].mult(m.matrix[1][1]));
        return mat;
    }

    public RationalFraction det(){
        return this.matrix[0][0].mult(this.matrix[1][1]).sub(this.matrix[1][0].mult(this.matrix[0][1]));
    }

    public RationalVector2D multVector(RationalVector2D rationalVector2D) {
        RationalVector2D result = new RationalVector2D(this.matrix[0][0].mult(rationalVector2D.getX()).add(this.matrix[0][1].mult(rationalVector2D.getY())),
                this.matrix[1][0].mult(rationalVector2D.getX().add(this.matrix[1][1].mult(rationalVector2D.getY()))));
        return result;
    }

    public int compareTo(RationalMatrix2x2) {
        this.matrix =
    }
}
