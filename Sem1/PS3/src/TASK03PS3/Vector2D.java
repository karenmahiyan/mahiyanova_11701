package TASK03PS3;
package cwork1;

import java.util.Comparator;
import java.util.Vector;

public class Vector2D {
    public double x;
    public double y;

    public Vector2D() {
        this.x = 0;
        this.y = 0;
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public Vector2D add(Vector2D vector2D) {
        return new Vector2D(this.getX() + vector2D.getX(), this.getY() + vector2D.getY());
    }

    public Vector2D sub(Vector2D vector2D) {
        return new Vector2D(this.getX() - vector2D.getX(), this.getY() - vector2D.getY());
    }

    public Vector2D mult(double num) {
        return new Vector2D(this.getX() * num, this.getY() * num);
    }

    public String toString() {
        return ("(" + getX() + ";" + getY() + ")");
    }

    public double length() {
        return Math.sqrt(this.getX() * this.getX() + this.getY() * this.getY());
    }

    public double scalarProduct(Vector2D vector2D) {
        return (this.getX() * vector2D.getX() + this.getY() * vector2D.getY());
    }

    public static void main(String[] args) {
        Vector2D vector1 = new Vector2D(3, 7);
        Vector2D vector2 = vector1.add(vector1);
        vector2 = vector2.sub(vector1);
        vector1.mult(3.0);
        System.out.println(vector1.toString());
        System.out.println(vector1.length());
        System.out.println(vector1.scalarProduct(vector2));
    }
    }
}