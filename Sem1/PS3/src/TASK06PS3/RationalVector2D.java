package TASK06PS3;

import TASK04PS3.RationalFraction;

public class RationalVector2D {
    private RationalFraction x;
    private RationalFraction y;
    public RationalVector2D(){
        this.x = new RationalFraction();
        this.y = new RationalFraction();
    }

    public RationalVector2D(RationalFraction a, RationalFraction b) {
        this.x = a;
        this.y = b;
    }

    public RationalFraction getX() {
        return x;
    }

    public void setX(RationalFraction x) {
        this.x = x;
    }

    public RationalFraction getY() {
        return y;
    }

    public void setY(RationalFraction y) {
        this.y = y;
    }

    public RationalVector2D add(RationalVector2D rationalVector2D){
        return new RationalVector2D(this.getX().add(rationalVector2D.getX()),this.getY().add(rationalVector2D.getY()));
    }

    public String toString(){
        return  "("+this.getX().toString()+";"+this.getY().toString()+")";
    }

    public double length(){
        return Math.sqrt(this.getX().mult(this.getX()).value()+this.getY().mult(this.getY()).value());
    }

    public RationalFraction scalarProduct(RationalVector2D rationalVector2D){
        return (this.getX().mult(rationalVector2D.getX()).add(this.getY().mult(rationalVector2D.getY())));
    }

    public static void main(String[] args) {
        RationalFraction rationalFraction1 = new RationalFraction(1,2);
        RationalFraction rationalFraction2 = new RationalFraction(2,5);
        RationalVector2D rationalVector2D = new RationalVector2D(rationalFraction1,rationalFraction2);
        System.out.println(rationalVector2D.toString());
    }
}
