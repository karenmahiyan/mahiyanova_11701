import java.util.Scanner;

/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 1 Task 02
 */
public class Task04PS1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        while (n % 2 == 0) {
            n = n / 2;
            System.out.println("2");
        }
        for (int i = 3; i <= n; i += 2) {
            while (n % i == 0) {
                System.out.println(i);
                n /= i;
            }
        }
    }
}
