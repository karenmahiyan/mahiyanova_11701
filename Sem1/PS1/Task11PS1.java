import java.util.Scanner;

/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 1 Task 11
 */
public class Task11PS1 {

    public static void main (String [] args){

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        boolean b = false;
        boolean c = false;
        int a = 1;
        int d = 0;
        int [] array = new int[n];

        for (int j = 0; j<n; j++){
            array[j] = sc.nextInt();
        }
        if (array[n-2] < array[n-1] && array[n-1] % 2 == 0){
            d++;
        }
        if (array[0] > array[1] && array[0] % 2 == 0){
            d++;
        }
        while(a < n-1 && d != 3){
            b = check(array, a);
            if (b == true) {
                d++;
            }
            a++;
        }
        if (d == 2) {
            System.out.println("Yes");
        }
        else {
            System.out.println("No");
        }
    }
    public static boolean check (int array[], int i) {
        return array[i] > array[i+1] && array[i] > array[i-1] && array[i] % 2 == 0;
    }
}
