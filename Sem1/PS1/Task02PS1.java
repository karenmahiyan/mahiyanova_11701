import java.util.Scanner;

/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 1 Task 02
 */
public class Task02PS1 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int k = sc.nextInt();
        int a = 0;

        if (m > k) {
            a = k;
            k = m;
            m = a;
        }

        for (int i = m + 1; i < k; i++) {

            if (i % 3 == 0) {
                System.out.print(i + " ");
            }
        }
    }
}