import java.util.Scanner;

/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 1 Task 08
 */
public class Task08PS1 {
    public static void main (String [] args){

        Scanner sc = new Scanner (System.in);
        double x = sc.nextDouble();
        final double eps = 1e-9;
        int sign = 1;
        double deg = x;
        int k = 0;
        int factorial = 1;
        double a = x;
        double s = a;

        while (a > eps || a <- eps){
            sign *= -1;
            deg *= x*x*x*x;
            factorial *= (2*k-1)*(2*k);
            a = (sign*deg)/(factorial*(4*k+1));
            s += a;
            k++;
        }
        System.out.println(s);
    }
}
