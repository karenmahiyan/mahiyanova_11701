import java.util.Scanner;

/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 1 Task 09
 */
public class Task09PS1 {
    public static void main (String [] args){

        Scanner sc = new Scanner (System.in);
        double x = sc.nextDouble();
        final double eps = 1e-9;
        int sign = 1;
        int k = 0;
        double deg = x;
        int fact = 1;
        double a = x;
        double s = a;

        while (a > eps || a <- eps){
            sign *= -1;
            deg *= x*x;
            fact *= k;
            a = (sign*deg)/(fact*(2*k+1));
            s += a;
            k++;
        }
        System.out.println(s);
    }
}
