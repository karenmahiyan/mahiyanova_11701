import java.util.Scanner;

/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 1 Task 05
 */
public class Task05PS1 {
    public static void main (String [] args){

        Scanner sc = new Scanner (System.in);
        int n = sc.nextInt();
        int factorial = 1;
        int factorial2 = 2;
        double s = 0.5;

        for (int m = 2; m <= n; m++){
            factorial *= (m-1);
            factorial2 *= (2*m-1)*(2*m);
            s += (double)(factorial*factorial)/factorial2;
        }
        System.out.println(s);
    }
}
