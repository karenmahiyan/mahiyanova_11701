package kr1;

public class Task02 {

	public static void main(String[] args) {
		int n = 10;
		int[] a = {1, 1, 1, 12, 16, 20, 23, 25, 28, 30}; 
		int count = 0;
		double d = 0;
		double c = 0;
		double b = 0;
		for (int i = 0; i < n; i++) {
			b = factorial(i);
			c = (factorial(a[i]) - Math.pow(2, i));
			d = (b * b) - (4 * c);
			if (d >= 0) {
				count++;
			}
		}
		
		boolean onlyOdd = true;
		
		while (count != 0) {
			if (count % 2 == 0) {
			onlyOdd = false;
			}
			count = count / 10;
		}
		System.out.println(onlyOdd);
	}
	public static double factorial(int n) {
		if (n == 0) {
			return 1;
		}
		return n * factorial (n-1);
	}
}