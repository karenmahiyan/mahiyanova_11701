package kr1;

public class Task01 {

	public static void main(String[] args) {
		int n = 10;
		int[] a = {4, 8, 10, 12, 16, 20, 23, 25, 28, 30};
		int count = 0;
		
		for (int i = 0; i < n; i++) {
			if (a[i] % 5 == 0) {
				count++;
			}
		}
		boolean containsEven = false;
		
		while (count != 0) {
			if (count % 2 == 0)
				containsEven = true;
			count = count / 10;
		}
		System.out.println(containsEven);
	}

}