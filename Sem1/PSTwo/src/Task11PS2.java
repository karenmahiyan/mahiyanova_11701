/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 2 Task 11
 */
import java.util.Arrays;
import java.util.Scanner;

public class Task11PS2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String array[] = new String[n];
        for (int i = 0; i < n; i++) {
            array[i] = sc.next();
        }
        sort(array);
        System.out.println(Arrays.asList(array));

    }
    public static int compare(String s1, String s2) {
        int n1 = s1.length();
        int n2 = s2.length();
        int min = Math.min(n1, n2);
        for (int i = 0; i < min; i++) {
            char c1 = s1.charAt(i);
            char c2 = s2.charAt(i);
            if (c1 != c2) {
                return c1 - c2;
            }
        }
        return n1 - n2;
    }
    public static void sort(String[] array) {
        for (int i = 1; i < array.length-1; i++){
            for (int j = 0; j < array.length - i; j++) {
                if (compare(array[j], array[j+1]) > 0) {
                    String t = array[j+1];
                    array[j+1] = array[j];
                    array[j] = t;
                }
            }
        }
    }

}
