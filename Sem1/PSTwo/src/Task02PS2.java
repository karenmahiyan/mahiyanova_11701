/**
 *@author Makhiyanova Karina
 * 11-701
 * Problem Set 2 Task 02
 */
import java.util.regex.*;

public class Task02PS2 {
    public static void main(String[] args) {
        Pattern p = Pattern.compile("0|([-+])?([1-9][0-9]*)(\\.[0-9]*([1-9]|\\([0-9]*[1-9]+[0-9]*\\)))?|[-+]?0(\\,[0-9]*([1-9]|\\([0-9]*[1-9]+[0-9]*\\)))");
        Matcher m = p.matcher("-7");
        System.out.println(m.matches());
    }
}
