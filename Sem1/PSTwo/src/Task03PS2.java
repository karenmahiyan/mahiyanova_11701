/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 2 Task 03
 */
import java.util.regex.Pattern;

public class Task03PS2 {
    public static void main(String[] args) {
        String[] s = new String[6];
        s[0] = "10101";
        s[1] = "000";
        s[2] = "00010";
        s[3] = "10111";
        s[4] = "0110";
        s[5] = "111";
        Pattern pattern = Pattern.compile("[0-9]");
        for (int i = 0; i < s.length; i++) {
            boolean b = s[i].matches("([0]*)|([1]*)|(((01)*0?)|((10)*1?))");
            if ( b == true) {
                System.out.println(i);
            }
        }
    }
}