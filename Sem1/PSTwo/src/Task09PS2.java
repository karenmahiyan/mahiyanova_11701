import java.util.Scanner;

/**
*@author Makhiyanova Karina
* 11-701
* Problem Set 2 Task 9
*/
public class Task09PS2{

    public static int a;
    public static int b;

    public static void main(String [] args){
        Scanner sc = new Scanner(System.in);
        a = sc.nextInt();
        b = sc.nextInt();
        String s = "";
        backTrack(s);
    }

    public static void backTrack(String s){
        if (s.length() == a){
            System.out.println(s);
        }
        else if (s.length()==0){
            for (char c = 'a'; c <= 'z'; c+=1){
                backTrack(s+c);
            }
        }
        else{
            for (char c = 'a'; c <= 'z'; c+=1){
                char c2 = s.charAt(s.length()-1);
                if ((c-c2)> b){
                    backTrack(s+c);
                }
            }
        }
    }
}