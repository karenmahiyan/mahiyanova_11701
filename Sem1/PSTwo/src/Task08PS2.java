
/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 2 Task 08
 */
import java.util.Scanner;

public class Task08PS2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String[] s = new String[2];
        s[0] = "842";
        s[1] = "452";
        int length = 0;
        for (int i = 0; i < s.length; i++) {
            length = s[i].length();

            if (length == n && backtrack(new String[]{s[i]}) != 1) {
                System.out.println(s[i]);
            }
        }
    }
    private static int backtrack(String[] s) {
        int a = 0;
        int b = 0;
        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < s[i].length() - 1; j++) {
                a = s[i].charAt(j);
                b = s[i].charAt(j + 1);
                while (a != b) {
                    if (a > b) {
                        a = a - b;
                    } else {
                        b = b - a;
                    }
                }

            }

        }
        return a;
    }
}
