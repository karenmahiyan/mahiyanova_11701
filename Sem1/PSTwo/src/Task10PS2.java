/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 2 Task 10
 */
import java.util.Scanner;

public class Task10PS2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s1 = sc.next();
        String s2 = sc.next();
        compare(s1, s2);
        if (compare(s1, s2) > 0) {
            System.out.println(s1);
            System.out.println(s2);
        }
        else {
            System.out.println(s2);
            System.out.println(s1);
        }

    }

    public static int compare(String s1, String s2) {
        int d = s1.length();
        int r = s2.length();
        int m = Math.min(d, r);
        for (int i = 0; i < m; i++) {
            char c1 = s1.charAt(i);
            char c2 = s2.charAt(i);
            if (c1 != c2) {
                return c1 - c2;
            }
        }
        return (d - r);
    }
}
