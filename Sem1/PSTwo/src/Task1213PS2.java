/**
 * @author Makhiyanova Karina
 * 11-701
 * Problem Set 2 Task 1213
 */
import java.util.Arrays;
import java.util.Random;

public class Task1213PS2 {
    public static void main(String[] args) {
        String[] array = new String[100];
        Random r = new Random();
        for (int i = 0; i < array.length; i++) {
            String s = "";
            for (int j = 0; j < 5; j++) {
                char c = ((char) (r.nextInt(26) + 'a'));
                s += c;
            }
            array[i] = s;
        }
        while (array.length != 0) {
            int count = 0;
            func(array);
            for (int i = 0; i < array.length; i++) {
                Arrays.sort(array);
                if (!array[i].equals("")) {
                    System.out.println(array[i]);
                    count++;
                }
            }
            String[] tmp = new String[count];
            int c = 0;
            for (int i = 0; i < array.length; i++) {
                Arrays.sort(array);
                if (!array[i].equals("")) {
                    tmp[c] = array[i];
                    c++;
                }
            }
            array = cross(tmp);
        }
    }

    public static void func(String[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean flag = true;
            for (int j = 0; j < array[i].length() - 1; j++) {
                if (array[i].charAt(j) - array[i].charAt(j + 1) < 10 &&array[i].charAt(j) - array[i].charAt(j + 1) >= 0) {
                    flag = false;
                }
            }
            if (flag == false) {
                array[i] = "";
            }
        }
    }

    public static String[] cross(String[] array) {
        String[] childArray = new String[array.length / 2];
        for (int i = 0; i < array.length / 2; i++) {
            if (!array[i].equals("") && !array[array.length - i-1].equals("")) {
                String temp = "";
                temp = temp + array[i].charAt(0) + array[i].charAt(1) +
                        array[i].charAt(3) + array[array.length - i-1].charAt(2) + array[array.length - i-1].charAt(4);
                childArray[i] = temp;
            }
        }
        return childArray;
    }

}
