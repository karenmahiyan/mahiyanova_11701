import java.util.Random;
import java.util.Arrays;


public class Genetics {

    public final static int L = 5;
    public final static int D = 10;

    public static void main(String[] args) {
        String [] str = new String [200];
        int sel = -1;
        newPopulation(str);
        System.out.println("New population:");
        System.out.println(Arrays.toString(str));
        for (int i = 0; i < 100 & sel != 0 ; i++) {
            evolve(str);
            sel = selection(str);
            System.out.println(i + " population:");
            System.out.println(Arrays.toString(str));
        }
    }

    public static void evolve(String[] a) {
        String s1;
        String s2;
        Random random = new Random();
        for (int i = 0; i < a.length; i++) {
            if (a[i].equals("")) {
                int random1 = random.nextInt(200);
                int random2 = random.nextInt(200);
                while (a[random1].equals("") || a[random2].equals("")) {
                    random1 = random.nextInt(200);
                    random2 = random.nextInt(200);
                }
                s1 = a[random1];
                s2 = a[random2];
                a[i] = hybrid(s1, s2);
            }
        }
    }

    public static int selection(String[] a) {
        int res = 0;
        for (int i = 0; i < 199; i++) {
            if (!a[i].equals("")) {
                if (function(a[i]) > 0) {
                    int j = i;
                    while (!a[j].equals("") && j < 199) {
                        a[j] = a[j + 1];
                        j++;
                    }
                    a[j] = "";
                    res++;
                }
            }
        }
        return res;
    }

    public static String hybrid(String s1, String s2) {
        Random rand = new Random();
        String result = "";
        for (int i = 0; i < L; i++) {
            if (rand.nextInt(2) == 0) {
                result += s1.charAt(i);
            }
            else {
                result += s2.charAt(i);
            }
        }
        return result;
    }
    
    public static void newPopulation (String[] a) {
        Random r = new Random();
        String s = "";
        for (int i = 0; i < (a.length / 2); i++) {
            for (int j = 0; j < L; j++) {
                s += (char) (r.nextInt(26) + 'a');
            }
            a[i] = s;
            s = "";
        }
        for (int i = (a.length / 2); i < a.length; i++) {
            a[i] = "";
        }
    }
    
    
    public static int function(String s) {
        int result = 0;
        for (int i = 0; i < L - 1; i++) {
            if (Math.abs(s.charAt(i) - s.charAt(i + 1)) <= D) {
                result++;
            }
        }
        return result;
    }
}