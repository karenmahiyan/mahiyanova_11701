package TASK11PS3;


public abstract class MordorsSoldier {
    private String name;

    public MordorsSoldier(String name) {
        this.name = name;
    }

    public void roar(){
        System.out.println("I am Mordor-soldier!");
    };
}
