package TASK11PS3;


public class Main {
    public static void main(String[] args) {
        MordorsSoldier[] soldiers = new MordorsSoldier[2];
        soldiers[0] = new Goblin("Misha");
        soldiers[1] = new Orc("Ghark");
        for(int i = 0;i<soldiers.length;i++){
            soldiers[i].roar();
        }
    }
}
