package TASK08PS3;


import java.util.HashMap;

public class Browser {
    private String name;
    HashMap<String, Double> sites;

    public Browser(String name, HashMap<String, Double> sites) {
        this.name = name;
        this.sites = sites;
    }

    public String getName() {
        return name;
    }

    public HashMap<String, Double> getSites() {
        return sites;
    }


}
