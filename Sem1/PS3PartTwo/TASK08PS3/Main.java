package TASK08PS3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("src/TASK08PS3/Browser_Speed.csv"));
        Browser[] browsers = new Browser[4];
        String t = sc.nextLine();
        String[] site = t.split(",");
        sc.nextLine();
        for (int i = 0; i < browsers.length; i++) {
            String parse = sc.nextLine();
            String[] parser = parse.split(",");
            HashMap<String, Double> sites = new HashMap<>();
            for (int j = 3; j < site.length; j++) {
                sites.put(site[j], Double.parseDouble(parser[j]));
            }
            browsers[i] = new Browser(parser[0], sites);
        }
        double min = Double.MAX_VALUE;
        int ind = 0;
        System.out.println("Самый быстрый сайт в среднем:");
        System.out.println();
        for (int j = 3; j < site.length; j++) {
            double res = 0;
            for (int i = 0; i < browsers.length; i++) {
                res += browsers[i].getSites().get(site[j]);
            }
            res = res / 4;
            if (res < min) {
                min = res;
                ind = j;
            }
        }
        System.out.println(site[ind]);

        min = Double.MAX_VALUE;
        ind = 0;
        System.out.println("Самый быстрый браузер в среднем:");
        System.out.println();
        for (int i = 0; i < browsers.length; i++) {
            double res = 0;
            for (int j = 3; j < site.length; j++) {
                res += browsers[i].getSites().get(site[j]);
            }
            if (res < min) {
                min = res;
                ind = i;
            }
        }
        System.out.println(browsers[ind].getName());

        for (int i = 0; i < browsers.length; i++) {
            double res = 0;
            for (int j = 3; j < site.length; j++) {
                res += browsers[i].getSites().get(site[j]);
            }
            res = res / 9;
            System.out.println("В среднем " + browsers[i].getName() + " открывает сайт за " + res);

        }
        System.out.println();
        for (int j = 3; j < site.length; j++) {
            double res = 0;
            for (int i = 0; i < browsers.length; i++) {
                res += browsers[i].getSites().get(site[j]);
            }
            res = res / 4;
            System.out.println("В среднем " + site[j] + " открывается за " + res);
        }
    }
}
