package TASK12PS3;


import java.math.BigInteger;

public class SimpleLongNumber implements Number {
    private long number;

    public SimpleLongNumber(long number) {
        this.number = number;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    @Override
    public Number add(Number n) {
        VeryLongNumber result = null;
        BigInteger k = BigInteger.valueOf(this.getNumber());
        if (n.getClass() == this.getClass()) {
            BigInteger sec = BigInteger.valueOf(((SimpleLongNumber) n).getNumber());
            k = k.add(sec);
            result = new VeryLongNumber(k.toString());
        } else {
            String s = ((VeryLongNumber) n).getNumber();
            k = k.add(new BigInteger(s));
            result = new VeryLongNumber(k.toString());
        }
        return result;
    }

    @Override
    public Number sub(Number n) throws NotNaturalNumberException {
        VeryLongNumber result = null;
        BigInteger k = BigInteger.valueOf(this.getNumber());
        if (n.getClass() == this.getClass()) {
            BigInteger sec = BigInteger.valueOf(((SimpleLongNumber) n).getNumber());
            if (k.compareTo(sec) < 0) {
                NotNaturalNumberException e = new NotNaturalNumberException("First number less than the second");
                throw e;
            }
            k = k.subtract(sec);
            result = new VeryLongNumber(k.toString());
        } else {
            byte[] s = ((VeryLongNumber) n).getNumber().getBytes();
            if (k.compareTo(new BigInteger(s)) < 0) {
                NotNaturalNumberException e = new NotNaturalNumberException("First number less than the second");
                throw e;
            }
            k = k.subtract(new BigInteger(s));
            result = new VeryLongNumber(k.toString());
        }
        return result;
    }

    @Override
    public int compareTo(Number n) {
        VeryLongNumber result = null;
        BigInteger k = BigInteger.valueOf(this.getNumber());
        if (n.getClass() == this.getClass()) {
            BigInteger sec = BigInteger.valueOf(((SimpleLongNumber) n).getNumber());
            if (k.compareTo(sec) < 0) {
                return -1;
            } else if (k.compareTo(sec) > 0) {
                return 0;
            } else return 1;
        } else {
            byte[] s = ((VeryLongNumber) n).getNumber().getBytes();
            if (k.compareTo(new BigInteger(s)) < 0) {
                return -1;
            } else if (k.compareTo(new BigInteger(s)) > 0) {
                return 0;
            } else return 1;
        }
    }
}
