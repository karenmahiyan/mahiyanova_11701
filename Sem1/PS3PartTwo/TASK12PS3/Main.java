package TASK12PS3;


public class Main {
    public static void main(String[] args) {
        Number[] numbers = new Number[3];
        numbers[0] = new SimpleLongNumber(500);
        numbers[1] = new SimpleLongNumber(500);
        numbers[2] = new VeryLongNumber("10000000000000");
        Number result = new SimpleLongNumber(0);
        for (int i = 0; i < numbers.length; i++){
            result = result.add(numbers[i]);
        }
        System.out.println(((VeryLongNumber) result).getNumber());
    }
}
