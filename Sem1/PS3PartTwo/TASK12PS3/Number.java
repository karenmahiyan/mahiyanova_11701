package TASK12PS3;


public interface Number {
    public Number add(Number n);
    public Number sub(Number n) throws NotNaturalNumberException;
    public int compareTo(Number n);
}
