package TASK12PS3;

public class NotNaturalNumberException extends Exception {
    public NotNaturalNumberException(String message){
        super(message);
    }
}
