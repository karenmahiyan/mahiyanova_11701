package TASK0910PS3;


public class User {
    private String username;
    private String password;
    private Gender gender;
    private String email;

    public User(String password, String username, Gender gender, String email) {
        this.password = password;
        this.username = username;
        this.gender = gender;
        this.email = email;
    }
}
