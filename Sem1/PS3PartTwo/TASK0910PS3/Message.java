package TASK0910PS3;


public class Message {
    private User sender;
    private User receiver;
    private String timesent;
    private String text;
    private Status status;

    public Message(User sender, User receiver, String timesent, String text, Status status) {
        this.sender = sender;
        this.receiver = receiver;
        this.timesent = timesent;
        this.text = text;
        this.status = status;
    }
}
