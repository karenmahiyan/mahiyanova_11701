import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {

    }

    public static LinkedList<Integer> founder(int N) {
        LinkedList<Integer> founded = new LinkedList<>();
        boolean isFive = true;
        boolean isThree = true;
        boolean isTwo = true;
        int outWay = 0;
        int way = 0;
        int inWay = 0;
        int out = 1;
        int in;
        int cross;
        while (isFive) {
            if (out < N) {
                founded.add(out);
            } else {
                isFive = false;
            }
            cross = out;
            while (isThree && isFive) {
                in = cross;
                while (isTwo) {
                    if ((in *= 2) < N) {
                        founded.add(in);
                    } else {
                        isTwo = false;
                    }
                }
                if ((cross *= 3) < N) {
                    isTwo = true;
                } else {
                    isThree = false;
                }
            }
            out *= 5;
        }
        return founded;
    }

}
