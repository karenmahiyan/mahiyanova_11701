import java.util.LinkedList;
import java.util.NoSuchElementException;

public class Queue <T> {

    private LinkedList <T> queue;

    public Queue() {
        queue = new LinkedList<>();
    }

    public boolean add (T element){
        return queue.add(element);
    }

    public T element(){
        T el = queue.getFirst();
        if(el != null){
            return el;
        } else{
            throw new NoSuchElementException();
        }
    }

    public T remove (){
        T el = queue.getFirst();
        if (el!=null) {
            queue.remove(el);
            return el;
        }else{
            throw new NoSuchElementException();
        }
    }

    public T peek (){
        return queue.getFirst();
    }

    public boolean offer(T element){
        queue.addLast(element);
        return true;
    }

    public T poll(){
        T el = queue.getFirst();
        if(el == null) return null;
        return el;
    }
}
