import java.util.ArrayList;

public class Main {
    final static int nOfThreads = 3;

    public static void main(String[] args) {
        String[] files = new String[5];
        String[] copied = new String[5];
        for (int i = 0; i < files.length; i++) {
            files[i] = "file" + i + ".txt";
            copied[i] = "copied" + i + ".txt";
        }

        ArrayList<Thread> pool = new ArrayList<>();
        for (int i = 0; i < nOfThreads; i++) {
            pool.add(new Thread());
        }

        int currentFile = 0;
        while (currentFile < files.length) {
            for (Thread thread: pool) {
                if (!thread.isAlive()) {
                    thread = new Thread(new Copy(files[currentFile], copied[currentFile]));
                    thread.start();
                }
            }
            currentFile++;
        }


    }
}
