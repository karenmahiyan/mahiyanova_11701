import java.io.*;

public class Task2 {
    public static void main(String[] args) {
        String[] files = {"file0.txt", "file1.txt", "file2.txt", "file3.txt", "file4.txt"};
        for (int i = 0; i < files.length; i++) {
            copyFile(files[i], "copied" + i + ".txt");
        }
    }

    public static void copyFile(String fileName, String copied) {
        FileInputStream is = null;
        try {
            is = new FileInputStream(new File("res/files/" + fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream os = new FileOutputStream(new File("res/copied/" + copied));
            try {
                byte[] buffer = new byte[4096];
                int length;
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
