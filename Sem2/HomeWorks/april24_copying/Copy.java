import java.io.*;

public class Copy implements Runnable {
    private String file;
    private String copied;

    public Copy(String file, String copied) {
        this.file = file;
        this.copied = copied;
    }

    @Override
    public void run() {
        copyFile(file, copied);
    }

    public void copyFile(String fileName, String copied) {
        FileInputStream is = null;
        try {
            is = new FileInputStream(new File("res/files/" + fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            FileOutputStream os = new FileOutputStream(new File("res/copied/" + copied));
            try {
                byte[] buffer = new byte[4096];
                int length;
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
