public interface TVAdapter {
    void SwitchOn();
    void SwitchOff();
    void ChangeChannel(int num);
    void changeSound(int num);
}
