public class SmartPhone implements TVAdapter{
    TVAdapter impl;

    public SmartPhone(TVAdapter impl) {
        this.impl = impl;
    }

    @Override
    public void SwitchOn() {
        impl.SwitchOn();
    }

    @Override
    public void SwitchOff() {
        impl.SwitchOff();
    }

    @Override
    public void ChangeChannel(int num) {
        impl.ChangeChannel(num);
    }

    @Override
    public void changeSound(int num) {
        impl.changeSound(num);
    }
}

