public class TVAdapterImpl implements TVAdapter {
    boolean isAlive;
    int currentChannel;
    int sound;

    public TVAdapterImpl(boolean isAlive, int currentChannel, int sound) {
        this.isAlive = isAlive;
        this.currentChannel = currentChannel;
        this.sound = sound;
    }

    @Override
    public void SwitchOn() {
        if (!isAlive) isAlive = true;
        else throw new IllegalStateException();
    }

    @Override
    public void SwitchOff() {
        if (isAlive) isAlive = false;
        else throw new IllegalStateException();
    }

    @Override
    public void ChangeChannel(int num) {
        if (currentChannel != num) currentChannel = num;
        else throw new IllegalStateException();
    }

    @Override
    public void changeSound(int num) {
        if (sound + num >= -100 && sound + num <= 100) {
            sound += num;
        } else throw new IllegalStateException();
    }
}
