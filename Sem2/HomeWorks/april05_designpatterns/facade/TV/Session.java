public class Session {
    private boolean connection;

    public Session(boolean connection) {
        this.connection = connection;
    }

    public boolean isConnection() {
        return connection;
    }
}
