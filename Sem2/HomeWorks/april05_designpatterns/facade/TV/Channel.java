public class Channel {
    int num;
    String name;
    String currentProgram;

    public Channel(int num, String name, String currentProgram) {
        this.num = num;
        this.name = name;
        this.currentProgram = currentProgram;
    }

    public void showCurrent(){
        System.out.println("Я показываю " + currentProgram);
    }
}
