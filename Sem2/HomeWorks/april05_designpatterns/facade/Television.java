import ru.itis.facade.TV.Channel;
import ru.itis.facade.TV.Session;
import ru.itis.facade.TV.TVAdapter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Television {
    private ArrayList<Channel> channels;
    private int current;
    private TVAdapter adapter;
    private Session session;
    private boolean isAlive;
    private Channel currentChannel;

    public Television(int current, TVAdapter adapter, Session session, ArrayList <Channel> channels) {
        this.adapter = adapter;
        this.session = session;
        if(session.isConnection()) this.current = current;
        else throw new IllegalStateException();
        isAlive = true;
        this.channels = channels;
        currentChannel = channels.get(current);
    }

    public void showSMTH(){
        Scanner in = new Scanner(System.in);
        boolean doYouWannaChangeChannel = false;
        while (true){
            currentChannel.showCurrent();
            if(doYouWannaChangeChannel = in.hasNextBoolean()){
                adapter.setNum(in.nextInt());
                channels.get(adapter.getNum());
            }
        }
    }

    public void switchOff(){
        isAlive = false;
    }
}
