import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class ChannelBase implements ChannelInfo{
    LinkedList<Channel>channels;

    public ChannelBase() {
    }

    public void addChannel(Channel channel){
        channels.add(channel);
        System.out.println(getNum(channel) + " " + getName(channel) + " " + Arrays.toString(getNextPrograms(channel).toArray()));
    }

    @Override
    public String getName(Channel channel) {
        return channel.getCurrent();
    }

    @Override
    public int getNum(Channel channel) {
        return channel.getNum();;
    }

    @Override
    public LinkedList<String> getNextPrograms(Channel channel) {
        return channel.getProgram();
    }
}
