import java.util.LinkedList;

public interface ChannelInfo {
    String getName(Channel channel);
    int getNum(Channel channel);
    LinkedList<String>getNextPrograms(Channel channel);
}
