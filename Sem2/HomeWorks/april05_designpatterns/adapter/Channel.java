import java.util.LinkedList;

public class Channel {
    private LinkedList<String> program;
    private int num;
    private String current;

    public Channel(LinkedList<String> program, int num, String current) {
        this.program = program;
        this.num = num;
        this.current = current;
    }

    public LinkedList<String> getProgram() {
        return program;
    }

    public int getNum() {
        return num;
    }

    public String getCurrent() {
        return current;
    }
}
