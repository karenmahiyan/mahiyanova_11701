import java.util.Iterator;
import java.util.LinkedList;

public class TVContentFactory {

    private LinkedList <String> dailyContent;
    private Iterator <String> content;

    public TVContentFactory(LinkedList<String> dailyContent) {
        this.dailyContent = dailyContent;
        content = dailyContent.iterator();
    }

    private class Content{
        String programName;

        public Content(String programName) {
            this.programName = programName;
        }

        public void generateContent(){
            System.out.println("Current content is " + programName);
        }
    }

    public Content getContent(){
        if(content.hasNext()){
            return new Content(content.next());
        }else {
            content = dailyContent.iterator();
            return new Content(content.next());
        }
    }
}
