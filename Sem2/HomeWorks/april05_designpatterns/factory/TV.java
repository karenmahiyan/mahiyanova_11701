import java.util.LinkedList;


public class TV {
    boolean connection;
    TVContentFactory currentChannel;

    public TV(boolean connection, LinkedList<String>programm) {
        this.connection = connection;
        currentChannel = new TVContentFactory(programm);
    }

    public void show(){
        while (connection){
            currentChannel.getContent();
        }
    }
}
