public class SmartTV extends TV{
    boolean hasConnection;

    public SmartTV(boolean hasConnection) {
        this.hasConnection = hasConnection;
    }

    public void internetConnection(){
        if (hasConnection){
            System.out.println("Нет подключения к инету");
        }
    }
}
