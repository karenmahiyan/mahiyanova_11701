package tasks;

import singletones.ContinentCollectionSingleton;
import entities.Continent;

import java.util.List;

public class MaxCountries {
    public static void main(String[] args) {
        System.out.println(biggestContinent().getName());
    }

    public static Continent biggestContinent(){
        List<Continent> continents = ContinentCollectionSingleton.getInstance();

        Continent biggestContinent;

        biggestContinent = continents
                .stream()
                .reduce((s1, s2) -> s1.CountriesCount() > s2.CountriesCount() ? s1:s2)
                .get();

        return biggestContinent;
    }
}