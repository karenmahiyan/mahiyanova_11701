package tasks;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordMaxUsage {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("res/en_v1.dic"));
        String finalWord = "";
        int maxValue = 0;

        while (sc.hasNext()) {
            String word = sc.next();
            int value = sc.nextInt();
            if(value > maxValue){
                maxValue = value;
                finalWord = word;
            }
        }
        System.out.println(finalWord +"  "+ maxValue);
    }
}
