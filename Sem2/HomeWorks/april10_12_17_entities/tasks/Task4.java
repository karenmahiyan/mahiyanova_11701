package tasks;

import singletones.CarMakerCollectionSingleton;
import entities.CarMaker;

import java.util.List;

public class Task4 {
    public static void main(String[] args) {

        List<CarMaker> carMakers = CarMakerCollectionSingleton.getInstance();
        System.out.println(
                carMakers.stream()
                        .filter(carMaker -> carMaker.countModelsBeforeYear(1975) > 0)
                        .count());

    }
}