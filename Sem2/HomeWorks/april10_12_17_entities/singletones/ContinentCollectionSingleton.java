package singletones;

import reader.Reader;
import entities.Continent;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class ContinentCollectionSingleton {
    private static final String FILE_NAME = "res/CARS/continents.csv";
    private static List<Continent> instance;

    public static List<Continent> getInstance() {
        if (instance == null) {
            instance = Reader.readContinents(FILE_NAME);
        }
        return instance;
    }
}