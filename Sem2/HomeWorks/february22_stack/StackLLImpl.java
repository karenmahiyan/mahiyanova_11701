import java.util.LinkedList;

public class StackLLImpl <T> implements Stack{
    LinkedList <T> stack;

    public StackLLImpl() {
        stack = new LinkedList<>();
    }

    @Override
    public boolean empty() {
        return stack.isEmpty();
    }

    @Override
    public Object pop() {
        Object current = stack.getFirst();
        stack.remove(current);
        return current;
    }

    @Override
    public boolean push(Object element) {
        stack.addFirst((T)element);
        return true;
    }
}
