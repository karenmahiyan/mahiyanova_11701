public interface Stack <T> {
    boolean empty();
    T pop();
    boolean push(T element);
}
