public class StackArrayImpl <T> implements Stack {
    T [] stack;
    int count;

    public StackArrayImpl(int size) {
        stack = (T[]) new Object [size];
        count = 0;
    }

    @Override
    public boolean empty() {
        return count == 0;
    }

    @Override
    public Object pop() {
        return stack[--count];
    }

    @Override
    public boolean push(Object element) {
        if(count<stack.length) {
            stack[count++] = (T) element;
            return true;
        }
        else return false;
    }
}
