import java.util.Stack;

public class Graph {

    private int VERTEX_SIZE = 100;
    private Vertex[] vertexList;
    private int vertexCount;
    private int[][] matrix;
    Stack<Integer> stack = new Stack();

    public Graph(){
        matrix = new int[VERTEX_SIZE][VERTEX_SIZE];
        for(int i = 0; i < VERTEX_SIZE; i++)
            for(int j = 0; j < VERTEX_SIZE; j++)
                matrix[i][j] = 0;
        vertexCount = 0;
        vertexList = new Vertex[VERTEX_SIZE];
    }

    public void addVertex(String label){
        vertexList[vertexCount++] = new Vertex(label);
    }

    public void addEdge(int begin, int end){
        matrix[begin][end] = 1;
        matrix[end][begin] = 1;
    }

    int getSuccessor (int v){
        for(int j = 0; j < vertexCount; j++)
            if(matrix[v][j] == 1 && vertexList[j].isVisited() == false)
                return j;
        return -1;
    }

    boolean isTree(){
        int edges = 0;
        int vertexes = 0;

        vertexList[0].setVisited(true);
        stack.push(0);

        while(!stack.isEmpty()){
            int current = stack.peek();
            int vertex = getSuccessor (current);
            if(vertex == -1)
                stack.pop();
            else
            {
                vertexList[vertex].setVisited(true);
                vertexes++;
                stack.push(vertex);
            }
        }
        for (int i = 0; i < vertexCount; ++i) {
            for (int j = i + 1; j < vertexCount; ++j) {
                if (matrix[i][j] != 0) {
                    edges++;
                }
            }
        }

        for(int j = 0; j < vertexCount; j++)
            vertexList[j].setVisited(false);

        if (edges != vertexCount - 1 & ++vertexes != vertexCount) {
            return false;
        }
        return true;
    }
}

