import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;


public class Summator {
    public class CharNum implements Comparable{
        private int numOf;
        private byte charNum;

        private CharNum(byte charNum) {
            this.charNum = charNum;
            numOf = 1;
        }

        public int getNumOf() {
            return numOf;
        }

        public byte getCharNum() {
            return charNum;
        }

        public void addChar (){
            numOf++;
        }

        @Override
        public int compareTo(Object o) {
            CharNum current = (CharNum)o;
            return charNum - current.charNum;
        }
    }

    private LinkedList <CharNum> chars;
    private Scanner file;

    public Summator(String fileName) {
        try {
            chars = new LinkedList<>();
            file = new Scanner(new File(fileName));
            save();
            sort();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void sort(){
        Collections.sort(chars);
    }

    public void show(){
        for (CharNum currentChar : chars) {
            System.out.println((char) currentChar.getCharNum() + ": " + currentChar.getNumOf());
            }
        }

    private void save (){
        String currentLine;
        boolean founded = false;
        while (file.hasNext()){
            currentLine = file.nextLine();
            char [] current = currentLine.toCharArray();
            for (char toCheck : current){
                if(Character.isLowerCase(toCheck)){
                    for (CharNum currentChar : chars){
                        founded = false;
                        if(currentChar.charNum == toCheck) {
                            currentChar.addChar();
                            founded = true;
                            break;
                        }
                    }
                    if(!founded){
                        chars.add(new CharNum((byte) toCheck));
                    }
                }
            }
        }

    }
}
