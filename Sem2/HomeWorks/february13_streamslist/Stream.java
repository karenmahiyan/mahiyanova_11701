import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

public interface Stream <E> {
    Stream <E> map (Function mapper);
    Stream <E> filter (Predicate predicate);
    Stream <E> sorted (Comparator <? super E> comparator);
}
