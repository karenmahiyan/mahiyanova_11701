import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class LinkedList<E> implements List<E> {

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }

    private class LinkedListIterator implements Iterator<E> {
        private Node current;

        private LinkedListIterator() {
            current = head;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public E next() {
            Node cross = current;
            current = current.next;
            return cross.value;
        }
    }

    public Stream<E> stream() {
        return new LinkedListStream();
    }

    private class LinkedListStream implements Stream<E> {
        private Iterator<E> iterator;

        private LinkedListStream() {
            iterator = iterator();
        }

        @Override
        public Stream<E> map(Function mapper) {
            LinkedList<E> currentList = new LinkedList<>();
            while (iterator.hasNext()) {
                currentList.add((E) mapper.apply(iterator.next()));
            }
            iterator = currentList.iterator();
            return this;
        }


        @Override
        public Stream<E> filter(Predicate predicate) {
            LinkedList<E> currentList = new LinkedList<>();
            E element;
            while (iterator.hasNext()) {
                element = (E) iterator.next();
                if (predicate.test(element)) {
                    currentList.add(element);
                }
            }
            iterator = currentList.iterator();
            return this;
        }

        @Override
        public Stream<E> sorted(Comparator<? super E> comparator) {
            java.util.LinkedList<E> currentList = new java.util.LinkedList<>();
            while (iterator.hasNext()) {
                currentList.add(iterator.next());
            }
            currentList.sort(comparator);
            iterator = currentList.iterator();
            return this;
        }
    }


    private class Node {

        E value;
        Node next;

        Node(E value) {
            this.value = value;
        }
    }

    private Node head;
    private Node tail;

    private int count;

    public LinkedList() {
        this.count = 0;
    }

    @Override
    public int indexOf(E element) {
        Node checking = head;
        for (int current = 0; current < count; current++) {
            if (head.value.equals(element)) return current;
            checking = checking.next;
        }
        return -1;
    }

    @Override
    public E get(int index) {
        Node currentElement = head;
        for (int current = 0; current < index; current++) {
            currentElement = currentElement.next;
        }
        return currentElement.value;
    }

    @Override
    public void addToBegin(E element) {
        Node newNode = new Node(element);
        newNode.next = head;
        head = newNode;
        count++;
    }

    @Override
    public void add(E element) {
        Node newNode = new Node(element);

        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.next = newNode;
            tail = newNode;
        }
        count++;
    }

    @Override
    public boolean remove(E element) {
        if (head == null) return false;
        count--;
        if (head == tail) {
            head = null;
            tail = null;
            count = 0;
            return true;
        }
        if (head.value.equals(element)) {
            head = head.next;
            return true;
        }

        Node newNode = head;

        while (newNode != null) {
            if (newNode.next.value == element) {
                if (tail == newNode.next) {
                    tail = newNode;
                }
                newNode.next = newNode.next.next;
                return true;
            }
            newNode = newNode.next;
        }
        count++;
        return false;
    }

    public void reverse() {
        LinkedList list = new LinkedList();
        Node current = head;
        while (current.next != null) {
            list.addToBegin(current.value);
            current = current.next;
        }
        list.addToBegin(current.value);
        head = list.head;
    }


    @Override
    public boolean contains(E element) {
        Node searcher = head;
        while (searcher.next != null) {
            if (searcher.value.equals(element)) return true;
            searcher = searcher.next;
        }
        return searcher.value.equals(element);
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void show() {
        Node current = head;
        while (current != null) {
            System.out.print(current.value + " ");
            current = current.next;
        }
        System.out.println();
    }

    public boolean clear() {
        if (count == 0) return false;
        else {
            count = 0;
            head = null;
            tail = null;
            return true;
        }
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public boolean containsAll(LinkedList<E> current) {
        Iterator<E> out = current.iterator();
        Iterator<E> thisCollection;
        boolean founded = false;
        while (out.hasNext()) {
            E founder = out.next();
            thisCollection = iterator();
            while (!founded && thisCollection.hasNext()) {
                if (thisCollection.next().equals(founder)) founded = true;
            }
            if (!founded) return false;
        }
        return true;
    }

    public boolean removeAll(LinkedList<E> deletable) {
        Iterator<E> toDelete = deletable.iterator();
        boolean deleted = true;
        while (toDelete.hasNext() && deleted) {
            deleted = remove(toDelete.next());
        }
        return deleted;
    }

    public boolean retainAll(LinkedList<E> current) {
        Iterator<E> toSave = current.iterator();
        E element;
        int count = 0;
        while (toSave.hasNext()) {
            if (!contains(element = toSave.next())) {
                remove(element);
                count++;
            }
        }
        return count > 0;
    }

    public boolean addAll(LinkedList<E> toAdd) {
        Iterator<E> current = toAdd.iterator();
        if (!current.hasNext()) return false;
        while (current.hasNext()) {
            add(current.next());
        }
        return true;
    }


    public void set(int index, E object) {
        if (index > count) return;
        Node a = head;
        Node newNodeToIndex = new Node(object);
        for (int i = 0; i < index - 1; i++) {
            a = a.next;
        }
        newNodeToIndex.next = a.next;
        a.next = newNodeToIndex;
    }


    public static <T extends Comparable<T>> LinkedList<T> merge(LinkedList<T> sorted1,
                                                                LinkedList<T> sorted2) {
        LinkedList<T> merged = new LinkedList<>();

        while (sorted1.head != null && sorted2.head != null) {
            if (sorted1.head.value.compareTo(sorted2.head.value) < 0) {
                merged.add(sorted1.head.value);
                sorted1.remove(sorted1.head.value);
            } else {
                merged.add(sorted2.head.value);
                sorted2.remove(sorted2.head.value);
            }
        }

        if (sorted1.head != null) {
            while (sorted2.head != null) {
                merged.add(sorted2.head.value);
                sorted2.remove(sorted2.head.value);
            }
        } else if (sorted2.head != null) {
            while (sorted1.head != null) {
                merged.add(sorted1.head.value);
                sorted1.remove(sorted1.head.value);
            }
        }

        return merged;
    }

}
