public interface Collection <T> {
    void addToBegin(T element);
    void add(T element);
    boolean remove(T element);
    boolean contains(T element);
    int size();
    void show();
}
