package downloading;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.Semaphore;

public class FileDownloader implements Runnable {
    private String fileURL = "";
    private Thread thread;
    private static Semaphore semaphore = new Semaphore(5, true);   


    public FileDownloader(String fileURL) {

        this.fileURL = fileURL;
        thread = new Thread(this);

    }


    public void download() {
        thread.start();

    }


    @Override
    public void run() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            URL urlObject = new URL(fileURL);
            InputStream is = urlObject.openStream();

            // creating fileName
            String[] URLpieces = fileURL.split("/");
            String copiedFileName = URLpieces[URLpieces.length - 1];


            FileOutputStream fos = new FileOutputStream(copiedFileName);
            byte[] buffer = new byte[4096];

            int data = is.read(buffer);
            while (data != -1) {
                fos.write(buffer, 0, data);
                data = is.read(buffer);
            }
            is.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        semaphore.release();
    }
}