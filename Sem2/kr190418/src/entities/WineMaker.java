package entities;

public class WineMaker {
    public final int id;
    public final String winery;
    public final Appelation appelation;
    public final Grape grape;
    public final String state; 
    public final String name; 
    public final String year;
    public final String price;
    public final String score; 
    public final String cases; 
    public final String drink;

    public WineMaker(int id, String winery, Appelation appelation, Grape grape, String state, String name, String year, String price, String score, String cases, String drink) {
        this.id = id;
        this.winery = winery;
        this.appelation = appelation;
        this.grape = grape;
        this.state = state;
        this.name = name; 
        this.year = year;
        this.price = price;
        this.score = score;
        this.cases = cases;
        this.drink = drink;
    }
}
