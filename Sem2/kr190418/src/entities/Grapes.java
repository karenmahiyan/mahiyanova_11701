package entities;

public class Grapes {
    public final int id;
    public final String grape;
    public final String color;

    public Appelation(int id, String grape, String color) {
        this.id = id;
        this.grape = grape;
        this.color = color;
    }
}
