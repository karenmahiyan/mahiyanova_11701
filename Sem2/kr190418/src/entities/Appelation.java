package entities;

public class Appelation {
    public final int id;
    public final String appelation;
    public final String county;
    public final String state;
    public final String area;
    public final String isAVA;

    public Appelation(int id, String appelation, String county, String state, String area, String isAVA) {
        this.id = id;
        this.appelation = appelation;
        this.county = county;
        this.state = state;
        this.area = area;
        this.isAVA = isAVA;
    }
}
