package collection.singletons;

import collection.Reader;
import entities.WineMaker;

import java.util.List;

public class WineMakerCollectionsSingleton {
    private static final String FILE_NAME = "res/WINES/wine.csv";
    private static List<CarMaker> instance;

    public static List<WIneMaker> getInstance() {
        if (instance == null) {
            instance = Reader.readWineMakers(FILE_NAME);
        }
        return instance;
    }
}