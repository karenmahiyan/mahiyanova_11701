package collection.singletons;

import collection.Reader;
import entities.Grapes;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class GrapesCollectionSingleton {
    private static final String FILE_NAME = "res/WINES/grapes.csv";
    private static List<Grapes> instance;

    public static List<Grapes> getInstance() {
        if (instance == null) {
            instance = Reader.readGrapes(FILE_NAME);
        }
        return instance;
    }
}
