import tasks.Task01;
import tasks.Task02;

public class TestWork
{
	public static void main( String[] args) {
		Task01 task01 = new Task01();
		Task02 task02 = new Task02();
		task01.taskOne();
		task02.taskTwo();
	}
}