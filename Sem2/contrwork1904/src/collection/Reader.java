package collection;

import collection.singletons.ContinentCollectionSingleton;
import collection.singletons.CountryCollectionSingleton;
import entities.CarMaker;
import entities.CarName;
import entities.Continent;
import entities.Country;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class Reader {
    private static String trim(String str) {
        return (String) Stream.of(str.split("'"))
                .filter((str1) -> !str1.equals(""))
                .toArray()[0]
        ;
    }


    public static List<WineMaker> readWineMakers(final String FILE_NAME) {
        List<WineMaker> wines;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            wines = new LinkedList<>();

            String line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");

                    Appelacion appelacion = null;
                    if (!strings[2].equals("null")) {
                        int appelaciontId = Integer.parseInt(strings[2]);
                        // find appelacion by id
                        appelacion = AppelacionCollectionSingleton.getInstance().stream().filter((appelacion1 -> appelacion1.id ==appelacionId)).findAny().get();
                    }

                    Grapes grape = null;
                    if (!strings[3].equals("null")) {
                        int grapetId = Integer.parseInt(strings[2]);
                        // find grape by id
                        grape = GrapesCollectionSingleton.getInstance().stream().filter((grape1 -> grape1.id ==grapeId)).findAny().get();
                    }

                    wines.add(
                            new Appelacion(
                                    Integer.parseInt(strings[0]),
                                    trim(strings[1]),
                                    appelacion,
                                    grape,
                                    trim(strings[4]),
                                    trim(strings[5]),
                                    trim(strings[6]),
                                    trim(strings[7]),
                                    trim(strings[8]),
                                    trim(strings[9]),
                                    trim(strings[10])
                            )
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            appelacions = null;
            e.printStackTrace();
        }
        return appelacions;
    }

    public static List<Grapes> readGrapes(final String FILE_NAME) {
        List<Grapes> grapes;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            grapes = new LinkedList<>();

            String line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");

                    int id = Integer.parseInt(strings[0]);
                    String grape = trim(strings[1]);
                    String color = trim(strings[2]);

                    Grapes grape = new Grapes(id, grape, color);
                    grapes.add(grape);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            grapes = null;
            e.printStackTrace();
        }
        return grapes;
    }

    public static List<Appelacion> readAppelacions(final String FILE_NAME) {
        List<Appelacion> appelacions;

        try {
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(FILE_NAME)
                    )
            );
            appelacions = new LinkedList<>();

            String line = bufferedReader.readLine();
            while (line != null) {
                try {
                    String[] strings = line.split(",");

                    int id = Integer.parseInt(strings[0]);
                    String appelation = trim(strings[1]);
                    String county = trim(strings[2]);
                    String state = trim(strings[2]);
                    String area = trim(strings[2]);
                    String isAVA = trim(strings[2]);

                    Appelacion appelacion = new Country(id, appelacion, county, state, area, isAVA);
                    appelacions.add(appelacion);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            appelacions = null;
            e.printStackTrace();
        }
        return appelacions;
    }

}
