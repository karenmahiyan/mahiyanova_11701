package collection.singletons;

import collection.Reader;
import entities.Appelation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

public class AppelationCollectionSingleton {
    private static final String FILE_NAME = "res/WINES/appelation.csv";
    private static List<Appelation> instance;

    public static List<Appelation> getInstance() {
        if (instance == null) {
            instance = Reader.readAppelations(FILE_NAME);
        }
        return instance;
    }
}
