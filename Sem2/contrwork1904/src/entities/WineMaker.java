package entities;

public class WineMaker {
    private int id;
    private String winery;
    private Appelation appelation;
    private Grape grape;
    private String state;
    private String name;
    private String year;
    private String price;
    private String score;
    private String cases;
    private String drink;

    public (int id, String winery, Appelation appelation, Grape grape, String state, String name, String year, String price, String score, String cases, String drink) {
        this.id = id;
        this.winery = winery;
        this.appelation = appelation;
        this.grape = grape;
        this.state = state;
        this.name = name; 
        this.year = year;
        this.price = price;
        this.score = score;
        this.cases = cases;
        this.drink = drink;
    }

    public int getId() {
        return id;
    }

    public String getWinery() {
        return winery;
    }

    public Appelation getAppelation() {
        return appelation;
    }

    public Grape getGrape() {
        return grape;
    }

    public String getState() {
        return state;
    }

    public String getName() {
        return name;
    }

    public String getYear() {
        return year;
    }

    public String getPrice() {
        return price;
    }

    public String getScore() {
        return score;
    }

    public String getCases() {
        return cases;
    }

    public String getDrink() {
        return drink;
    }
}
