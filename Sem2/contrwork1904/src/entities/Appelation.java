package entities;

public class Appelation {
    private  int id;
    private String appelation;
    private String county;
    private String state;
    private String area;
    private String isAVA;

    public Appelation(int id, String appelation, String county, String state, String area, String isAVA) {
        this.id = id;
        this.appelation = appelation;
        this.county = county;
        this.state = state;
        this.area = area;
        this.isAVA = isAVA;
    }

    public int getId() {
        return id;
    }

    public String getAppelation() {
        return appelation;
    }

    public String getCounty() {
        return county;
    }

    public String getState() {
        return state;
    }

    public String getArea() {
        return area;
    }

    public String getIsAVA() {
        return isAVA;
    }
}
