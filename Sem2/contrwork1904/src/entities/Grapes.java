package entities;

public class Grapes {
    private int id;
    private String grape;
    private String color;

    public Appelation(int id, String grape, String color) {
        this.id = id;
        this.grape = grape;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getGrape() {
        return grape;
    }

    public String getColor() {
        return color;
    }
}
