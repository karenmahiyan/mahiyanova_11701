package tasks;

import collection.singletons.WineMakerCollectionsSingleton;
import collection.singletons.GrapesCollectionsSingleton;
import entities.Grapes;

import java.util.List;

public class Task02 {

    public void taskTwo(){ 
        //вывод общего кол-ва вин по каждому из сортов

        Map<String, Integer> grapes = new HashMap<String, Integer>();

        for(WineMarker wine_marker : wine_marker_collection){ 
            if(!wine_marker.grape != null){
                if( !grapes.get(wine_marker.grape.grape) )      
                    grapes.put(wine_marker.grape.grape, 1);
                else{
                    int count = grapes.get(wine_marker.grape.grape);
                    grapes.remove(wine_marker.grape.grape);
                    grapes.put(wine_marker.grape.grape, count++);
                }
            }
        }

        grapes.entrySet().stream().forEach(e -> {
            System.out.println("Общее кол-во вина по каждому из сортов:");
            System.out.println("Название сорта: "+e.getKey()+" Кол-во: "+e.getValue());
        });
    }
}