package tasks;

import collection.singletons.WineMakerCollectionsSingleton;
import collection.singletons.AppelationCollectionsSingleton;
import entities.Appelation;

import java.util.List;

public class Task01 {

    public void taskOne(){ 
        //вывод места с наибольшим кол-во вин
        WineMakerCollectionsSingleton wine_markers = new WineMakerCollectionsSingleton(); 
        wine_marker_collection = wine_markers.getInstance();

        int max_count_wines = 0;
        String appelation_with_max_count_wines = null;
        Map<String, Integer> appelation = new HashMap<String, Integer>();

        for(WineMarker wine_marker : wine_marker_collection){ 
            if(wine_marker.appelation.name != null){
                if( !appelation.get(wine_marker.appelation.name) )      
                    appelation.put(wine_marker.appelation,name, 1);
                else{
                    int count = appelation.get(wine_marker.appelation.name);
                    appelation.remove(wine_marker.appelation.name);
                    appelation.put(wine_marker.appelation.name, count++);
                }
            }
        }

        appelation.entrySet().stream().forEach(e -> {
                if(max_count_wines<e.getValue()){
                    max_count_wines = e.getValue();
                    appelation_with_max_count_wines = e.getKey().name;
                }
        });


        System.out.println("Название места с наибольшим кол-во вина:");
        System.out.println(appelation_with_max_count_wines);
    }
}