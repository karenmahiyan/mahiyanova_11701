public class Main {

    public static void main(String[] args) {
        Dictionary dictionary = new Dictionary("Dictionary.txt");
        System.out.println("Показать словарь");
        dictionary.show();
        System.out.println();
        System.out.println();
        System.out.println("Удаление элемента");
        dictionary.remove("go");
        dictionary.remove("friends");
        dictionary.remove("me");
        dictionary.show();
        System.out.println();
        System.out.println();
        System.out.println(dictionary.numLen1());
        System.out.println();
        System.out.println();
        System.out.println("Перевод по словарю");
        System.out.println(dictionary.translate("фото сообщения игра блокнот"));
        System.out.println();
        System.out.println();
        System.out.println("Добавить элемент");
        dictionary.insert("тут", "here");
        dictionary.show();
    }
}