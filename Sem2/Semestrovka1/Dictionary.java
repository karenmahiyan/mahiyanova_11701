import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Dictionary {

    private static class Translations {
        Word value;
        Translations next;

        Translations(Word value) {
            this.value = value;
        }
    }

    private static class Word {
        String word;
        String translation;

        public Word(String word, String translation) {
            this.word = word;
            this.translation = translation;
        }

        public String getWord() {
            return word;
        }

        public String getTranslation() {
            return translation;
        }

        @Override
        public String toString() {
            return getWord() + " " + getTranslation();
        }
    }


    private Translations head;
    private Translations tail;

    public Dictionary(String fileName) {
        try {
            Scanner dictionary = new Scanner(new File(fileName));
            String currentString = dictionary.nextLine();
            String[] currentTranslation = currentString.split(" ");
            Translations newTranslation = new Translations(new Word(currentTranslation[0], currentTranslation[1]));
            head = newTranslation;
            tail = newTranslation;
            while (dictionary.hasNext()) {
                currentString = dictionary.nextLine();
                currentTranslation = currentString.split(" ");
                insert(currentTranslation[0], currentTranslation[1]);
            }
        }
        catch (FileNotFoundException e) {
            System.err.println("Не прочитан файл");
            throw new IllegalArgumentException(e);
        }
    }

    public void show() {
        Translations current = head;
        while (current.next != null) {
            System.out.println(current.value.toString());
            current = current.next;
        }
        System.out.print(current.value.toString());
    }


    public void remove(String k) {
        if (head == null) return;
        if (head == tail) {
            head = null;
            tail = null;
            return;
        }
        if (head.value.translation.equals(k)) {
            head = head.next;
            return;
        }

        Translations current = head;

        while (current != null) {
            if (current.next.value.translation.equals(k)) {
                if (tail == current.next) {
                    tail = current;
                }
                current.next = current.next.next;
                return;
            }
            current = current.next;
        }
    }

    public int numLen1() {
        int count = 0;
        Translations current = head;
        while (current != null) {
            int value1 = current.value.getTranslation().length();
            int value2 = current.value.getWord().length();
            if (value1 == value2 || value1 + 1 == value2 || value1 == value2 + 1) {
                count++;
            }
            current = current.next;
        }
        return count;
    }

    public String translate(String text) {
        String[] textToArr = text.split(" ");
        String translatedText = "";
        Translations all = head;
        for (String currentString: textToArr) {
            while (!all.value.getWord().equals(currentString)) {
                all = all.next;
            }
            translatedText += all.value.getTranslation() + " ";
            all = head;
        }
        return translatedText;
    }

    public void insert(String k, String v) {
        Translations newTranslation = new Translations(new Word(k, v));
        tail.next = newTranslation;
        tail = newTranslation;
    }
}