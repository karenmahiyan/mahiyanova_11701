import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

    class SortedByName implements Comparator <ArrayList<Integer>> {
         public int compare(ArrayList<Integer> obj1, ArrayList<Integer> obj2) {
             int str1 = obj1.size();
             int str2 = obj2.size();
             return str1 - str2;
         }
    }

    public class Main {
        private static int[] toArray(List<Integer> list) {
             int[] a = new int[list.size()];
             for (int i = 0; i < list.size(); i++) {
             a[i] = list.get(i);
             }
             return a;
        }

    public static void main(String[] args) throws FileNotFoundException {
//        double SEC = (double) 1e-9;
//        PrintWriter pwStat = new PrintWriter(new File("sizes.txt"));
//        ArrayFileReader reader = new ArrayFileReader(new File("arrays.txt"));
//        ArrayList <ArrayList<Integer>> data = reader.readData();
//        Collections.sort(data, new SortedByName());
//        for(int i = 0; i < data.size(); i++){
//            pwStat.println(data.get(i).size());
//        }
//        pwStat.close();
//        pwStat = new PrintWriter(new File("LinkedList.txt"));
//       for (int i = 0; i < data.size(); i++) {
//            LinkedList<Integer> list = new LinkedList<>(data.get(i));
//            long time = System.nanoTime();
//            ListSmoothSort.sort(list);
//            long time2 = System.nanoTime();
//            pwStat.println((time2 - time) * SEC);
//        }
//        pwStat.close();
//        pwStat = new PrintWriter(new File("ArrayList.txt"));
//        for(int i = 0; i < data.size(); i++) {
//            long time = System.nanoTime();
//            ListSmoothSort.sort(data.get(i));
//            long time2 = System.nanoTime();
//            pwStat.println((time2 - time) * SEC);
//        }
//        pwStat.close();
//        pwStat = new PrintWriter(new File("Array.txt"));
//        PrintWriter pw = new PrintWriter(new File("Iters.txt"));
//        for(int i = 0; i < data.size(); i++){
//            int [] a = toArray(data.get(i));
//            long time = System.nanoTime();
//            ArraySmoothSort.sort(a);
//            long time2 = System.nanoTime();
//            pwStat.println((time2 - time) * SEC);
//            pw.println(ArraySmoothSort.amountOfIters);
//        }
//        pwStat.close();
//        pw.close();
        ArrayGeneratorAndFileWriter gw = new ArrayGeneratorAndFileWriter(new File("Array.txt"));
    }
}