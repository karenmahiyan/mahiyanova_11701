import java.util.LinkedList;
import java.util.List;

public class ListSmoothSort {

    private static final int LP[] = { 1, 1, 3, 5, 9, 15, 25, 41, 67, 109, 177, 287, 465, 753, 1219, 1973, 3193, 5167, 8361, 13529, 21891, 35421, 57313, 92735,
            150049, 242785, 392835, 635621, 1028457, 1664079, 2692537, 4356617, 7049155, 11405773, 18454929, 29860703, 48315633, 78176337, 126491971,
            204668309, 331160281, 535828591, 866988873 };

    public static  void smoothSort(List<Integer> source, int startIndex, int endIndex) {
        int head = startIndex;
        int p = 1;
        int pshift = 1;
        while (head < endIndex) {
            if ((p & 3) == 3) {
                sift(source, pshift, head);
                p >>>= 2;
                pshift += 2;
            }
            else {
                if (LP[pshift - 1] >= endIndex - head) {
                    trinkle(source, p, pshift, head, false);
                }
                else {
                    sift(source, pshift, head);
                }
                if (pshift == 1) {
                    p <<= 1;
                    pshift--;
                }
                else {
                    p <<= (pshift - 1);
                    pshift = 1;
                }
            }
            p |= 1;
            head++;
        }
        trinkle(source, p, pshift, head, false);
        while (pshift != 1 || p != 1) {
            if (pshift <= 1) {
                int trail = Integer.numberOfTrailingZeros(p & ~1);
                p >>>= trail;
                pshift += trail;
            }
            else {
                p <<= 2;
                p ^= 7;
                pshift -= 2;
                trinkle(source, p >>> 1, pshift + 1, head - LP[pshift] - 1, true);
                trinkle(source, p, pshift, head - 1, true);
            }
            head--;
        }
    }

    private static void sift(List<Integer> source, int pshift, int head) {
        Integer val = source.get(head);
        while (pshift > 1) {
            int rt = head - 1;
            int lf = head - 1 - LP[pshift - 2];
            if (val >= source.get(lf) && val >= source.get(rt))
                break;
            if (source.get(lf) >= source.get(rt)) {
                source.set(head, source.get(lf));
                head = lf;
                pshift -= 1;
            }
            else {
                source.set(head, source.get(rt));
                head = rt;
                pshift -= 2;
            }
        }
        source.set(head, val);
    }

    private static void trinkle(List<Integer> source, int p, int pshift, int head, boolean isTrusty) {
        Integer val = source.get(head);
        while (p != 1) {
            int stepson = head - LP[pshift];
            if (source.get(stepson) <= val)
                break;
            if (!isTrusty && pshift > 1) {
                int rt = head - 1;
                int lf = head - 1 - LP[pshift - 2];
                if (source.get(rt) >= (source.get(stepson)) || source.get(lf) >= (source.get(stepson)) )
                    break;
            }
            source.set(head, source.get(stepson));
            head = stepson;
            int trail = Integer.numberOfTrailingZeros(p & ~1);
            p >>>= trail;
            pshift += trail;
            isTrusty = false;
        }
        if (!isTrusty) {
            source.set(head, val);
            sift(source, pshift, head);
        }
    }

    public static void sort(List<Integer> source) {
        smoothSort(source,0,source.size() - 1);
    }
}
