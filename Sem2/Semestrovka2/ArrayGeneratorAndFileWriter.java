import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

public class ArrayGeneratorAndFileWriter {
    final int MIN_SIZE = 100;
    final int MAX_SIZE = 10000;
    final int AMOUNT_ARRAYS = 60;
    Random random = new Random();
    PrintWriter pw;
    public ArrayGeneratorAndFileWriter(File file){
        try {
            pw = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void write(){
        for(int i = 0; i < AMOUNT_ARRAYS; i++){
            int curSize = random.nextInt(MAX_SIZE - MIN_SIZE) + MIN_SIZE;
            System.out.println(curSize);
            for(int g = 0; g < curSize; g++){
                pw.print(random.nextInt());
                pw.print(" ");
            }
            pw.println();
        }
        pw.close();
    }
}
