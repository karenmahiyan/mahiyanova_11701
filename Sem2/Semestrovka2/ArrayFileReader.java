import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ArrayFileReader {
    Scanner sc;
    ArrayList <ArrayList<Integer>> list;
    public ArrayFileReader(File file){
        list = new ArrayList<>();
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ArrayList <ArrayList<Integer>> readData(){
        while(sc.hasNextLine()){
            String[] arr = sc.nextLine().split(" ");

            ArrayList<Integer> list = new ArrayList<>();
            System.out.println(arr.length - 1);
            for(int i = 0; i < arr.length - 1; i++){
                list.add(Integer.parseInt(arr[i]));
            }
            this.list.add(list);
        }
        return list;
    }
}
