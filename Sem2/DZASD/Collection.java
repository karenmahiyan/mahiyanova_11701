package DZASD;

public interface Collection {
        void addToBegin(Object element);
        void add(Object element);
        void remove(Object element);
        boolean contains(Object element);
        int size();
}
